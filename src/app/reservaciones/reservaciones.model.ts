export class Reservaciones {
  constructor(
    public id: string,
    public restauranteId: string,
    public nombreRest: string,
    public nombre:string,
    public fecha: string,
    public imgUrl: string,
    public usuarioId: string
  ){}

}
