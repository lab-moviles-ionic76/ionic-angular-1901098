import { Component, OnInit, OnDestroy } from '@angular/core';
import { Reservaciones } from './reservaciones.model';
import { ReservacionesService } from './reservaciones.service';
import { Subscription } from 'rxjs';
import { IonItemSliding, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-reservaciones',
  templateUrl: './reservaciones.page.html',
  styleUrls: ['./reservaciones.page.scss'],
})
export class ReservacionesPage implements OnInit, OnDestroy {

  reservaciones: Reservaciones[];
  private reservacionesSub: Subscription;
  isLoading = false;

  constructor(
    private reservacionesService: ReservacionesService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.reservacionesSub = this.reservacionesService.reservaciones.subscribe(rsvs => {
      this.reservaciones = rsvs;
    });
  }

  ionViewWillEnter(){
    console.log('IONIC -> ionViewWillEnter');
    this.isLoading = true;
    //CONSUMIMOS EL SERVICIO PARA OBTENER LAS RESERVACIONES DEL USUARIO LOGGEADO
    this.reservacionesService.fetchReservaciones().subscribe(rsvs => {
      this.reservaciones = rsvs;
      this.isLoading = false;
    });
  }

  ngOnDestroy(){
    if(this.reservacionesSub){
      this.reservacionesSub.unsubscribe();
    }
  }

  onRemoveReservacion(reservacionId: string, slidingEl: IonItemSliding){
    slidingEl.close();
    this.loadingCtrl.create({
      message: 'eliminando reservacion ...'
    })
    .then(loadingEl => {
      loadingEl.present();
      this.reservacionesService.removeReservacion(reservacionId).subscribe(() => {
        loadingEl.dismiss();
      });
    });
  }


}
