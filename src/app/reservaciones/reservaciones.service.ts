import { Injectable } from "@angular/core";
import { Restaurante } from "../restaurantes/restaurante.model";
import { Reservaciones } from "./reservaciones.model";
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoginService } from '../login/login.service';


@Injectable({
  providedIn: 'root'
 })

export class ReservacionesService{
  //private reservaciones: Reservaciones[]=[
    //{id: 1, restauranteId: '1', nombreRest: 'Cars Jr', fecha: 'Domingo 14 de Marzo - 18:00hrs', imgUrl: 'https://frankata.com/wp-content/uploads/2019/01/800x600carlsjr-1170x877.jpg'},
    //{id: 2, restauranteId: '2', nombreRest: 'Super Salads', fecha: 'Jueves 1 de Abril - 14:30hrs', imgUrl: 'https://cdn.worldvectorlogo.com/logos/super-salads.svg'},
   // {id: 3, restauranteId: '3', nombreRest: 'Cabo Grill', fecha: 'Luunes 22 de Marzo - 20:00hrs', imgUrl: 'https://i.pinimg.com/280x280_RS/e3/1e/e7/e31ee7950607eb55c87e199fd5ab6dd7.jpg'},
  //];

  private _reservaciones = new BehaviorSubject<Reservaciones[]>([]);
  usuarioId = null;

  get reservaciones(){
    return this._reservaciones.asObservable();
  }

  fetchReservaciones(){
    let url = environment.firebaseURL + 'reservaciones.json?orderBy="usuarioId"&equalTo="'+ this.usuarioId + '"';
    return this.http.get<{[key: string] : Reservaciones}>(url)
    .pipe(map(dta =>{
      const rests = [];
      for(const key in dta){
        if(dta.hasOwnProperty(key)){
          rests.push(new Reservaciones(
            key,
            dta[key].restauranteId,
            dta[key].nombreRest,
            dta[key].nombre,
            dta[key].fecha,
            dta[key].imgUrl,
            dta[key].usuarioId
          ));
        }
      }
      return rests;
    }),
    tap(rest => {
      this._reservaciones.next(rest);
    }));
  }

  //constructor(){}

  //getAllReservaciones(){
    //return[...this.reservaciones];
  //}

  //getReservacion(reservacionId: number){
    //return {...this.reservaciones.find(r =>{
      //return r.id === reservacionId;
    //})};
  //}


  addReservacion(restaurante: Restaurante, nombre: string, horario: string){
    const rsv = new Reservaciones(
      null,
      restaurante.id,
      restaurante.titulo,
      nombre,
      horario,
      restaurante.imgUrl,
      this.usuarioId
    );

    this.http.post<any>(environment.firebaseURL + 'reservaciones.json', {...rsv}).subscribe(data => {
      console.log(data);
    });
  }


  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ){
    this.loginService.usuarioId.subscribe(usuarioId => {
      this.usuarioId = usuarioId;
    });
  }

  removeReservacion(reservacionId: string){
    return this.http.delete(`${environment.firebaseURL}reservaciones/${reservacionId}.json`)
    .pipe(switchMap(()=>{
      return this.reservaciones;
    }), take(1), tap(rsvs => {
      this._reservaciones.next(rsvs.filter(r => r.id !== reservacionId))
    }))
  }

  getReservacion(reservacionId: string){
    const url = environment.firebaseURL + `reservaciones/${reservacionId}.json`;

    return this.http.get<Reservaciones>(url)
    .pipe(map(dta => {
      return new Reservaciones(
        reservacionId,
        dta.restauranteId,
        dta.nombreRest,
        dta.nombre,
        dta.fecha,
        dta.imgUrl,
        dta.usuarioId
      );
    }));
  }

  updateReservacion(reservacionId: string, reservacion: Reservaciones){
    const url = environment.firebaseURL + `reservaciones/${reservacionId}.json`;

    this.http.put<any>(url, {...reservacion}).subscribe(data => {
      console.log(data);
    });
  }

}
