import { BehaviorSubject } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Ofertas } from "./ofertas.model";
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class OfertasService{

  constructor(private http: HttpClient){}


  private _ofertas = new BehaviorSubject<Ofertas[]>([]);
  ofertas1: Ofertas[] = [
    {id: null, titulo: 'MALTEADA DE CHOCOLATE A 20 PESOS', vigencia: 'Valido unicamente en el dia internacional del chocolate', imgUrl: 'https://static.promodescuentos.com/pepperpdimages/threads/thread_large/default/504920_1.jpg'},
    {id: null, titulo: 'TODOS LOS TACOS QUE QUIERAS POR SOLO 109 PESOS', vigencia: 'Vigencia hasta agotar existencia', imgUrl: 'https://mk0cazaofertassmxlbf.kinstacdn.com/wp-content/uploads/2014/12/cabo-grill-monterrey.jpg'},
    {id: null, titulo: 'DESAYUNOS 2X1 SABADOS Y DOMINGOS HASTA LAS 12 PM', vigencia: 'Vigencia hasta agotar existencia', imgUrl: 'https://mk0cazaofertassmxlbf.kinstacdn.com/wp-content/uploads/2017/01/super-salads-2x1-desayunos.jpg'},
  ];


  get ofertas(){
    return this._ofertas.asObservable();
  }

addOfertas(ofertas: Ofertas){
  this.http.post<any>(environment.firebaseURL + 'ofertas.json', {...ofertas}).subscribe(data => {
    console.log(data);
  });
}

fetchOfertas(){
  //this.addOfertas(this.ofertas1[0]);
  //this.addOfertas(this.ofertas1[1]);
  //this.addOfertas(this.ofertas1[2]);
  return this.http.get<{[key: string]: Ofertas}>(
    environment.firebaseURL + 'ofertas.json'
  )
  .pipe(map(dta =>{
    const rests = [];
    for(const key in dta){
      if(dta.hasOwnProperty(key)){
        rests.push(
          new Ofertas(key, dta[key].titulo, dta[key].vigencia, dta[key].imgUrl
        ));
      }
    }
    return rests;
  }),
  tap(rest => {
    this._ofertas.next(rest);
  }));
}

//getAllOfertas(){
  //this.addOfertas(this.ofertas[0]);
  //this.addOfertas(this.ofertas[1]);
  //this.addOfertas(this.ofertas[2]);
  //return [...this.ofertas];
//}

getOferta(ofertaId: string){
  const url = environment.firebaseURL + `ofertas/${ofertaId}.json`;
  return this.http.get<Ofertas>(url)
  .pipe(map(dta => {
    return new Ofertas(ofertaId, dta.titulo, dta.vigencia, dta.imgUrl);
  }));
}

}
