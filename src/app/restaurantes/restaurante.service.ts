import { BehaviorSubject } from 'rxjs';
import { Injectable } from "@angular/core";
import { Restaurante } from "./restaurante.model";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { map, tap } from "rxjs/operators";

@Injectable({
 providedIn: 'root'
})
export class RestauranteService{

  constructor(private http: HttpClient){}

 private _restaurantes = new BehaviorSubject<Restaurante[]>([]);

 get restaurantes(){
  return this._restaurantes.asObservable();
  }


 addRestaurante(restaurante: Restaurante){
    this.http.post<any>(environment.firebaseURL + 'restaurantes.json', {...restaurante}).subscribe(data => {
      console.log(data);
    });
  }

  fetchRestaurantes(){
   // this.addRestaurante(this.restaurantes[0]);
  //this.addRestaurante(this.restaurantes[1]);
  //this.addRestaurante(this.restaurantes[2]);
    return this.http.get<{[key: string] : Restaurante}>(
      environment.firebaseURL + 'restaurantes.json'
    )
    .pipe(map(dta =>{
      const rests = [];
      for(const key in dta){
        if(dta.hasOwnProperty(key)){
          rests.push(
          new Restaurante(key, dta[key].titulo, dta[key].imgUrl, dta[key].platillos, dta[key].lat, dta[key].lng
        ));
     }
    }
    return rests;
  }),
  tap(rest => {
    this._restaurantes.next(rest);
  }));
}




 getRestautante(restauranteId: string){
  const url = environment.firebaseURL + `restaurantes/${restauranteId}.json`;
  return this.http.get<Restaurante>(url)
  .pipe(map(dta => {
    return new Restaurante(restauranteId, dta.titulo, dta.imgUrl, dta.platillos, dta.lat, dta.lng);
  }));
 }

 deleteRestaurante(restauranteId: string){

  }


}
